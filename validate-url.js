/**
* @param ctx {WebtaskContext}
*/
"use latest";
require('isomorphic-fetch@2.2.0');

const checkUrl = (url) => {
  const hasHttp = url.substring(0, 8);
  if (hasHttp.indexOf('http://') === 0 || hasHttp.indexOf('https://') === 0) {
    return url;
  } else {
    return `https://${url}`;
  }
}

const messages = {
  urlExist : 'The url exists.',
  urlDoesnExist: 'URL does not appear to exist.',
  urlEmpty: 'The url value is required'
}

module.exports = function (ctx, cb) {
  if (ctx.data.url) {
    let url = checkUrl(String(ctx.data.url));
    fetch(url)
      .then((response) => {
        const res = {
          status: response.status,
          url: messages.urlExist
        };
        cb(null, res);
      })
      .catch((err) => {
        const res = {
          error: err,
          url: messages.urlDoesnExist
        };
        cb(null, res);
      });
  } else {
    cb(null, messages.urlEmpty);
  }
};